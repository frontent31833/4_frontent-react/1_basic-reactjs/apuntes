# INTRODUCCION REACT JS

[React wiki midudev](https://www.reactjs.wiki/)

## ¿Qué es un componente?

- Componente: Un componente es una porción de código independiente y reutilizable que permite modularizar y organizar el desarrollo de aplicaciones. Los componentes encapsulan tanto la estructura visual como la lógica relacionada en un mismo bloque, lo que facilita su mantenimiento, reutilización y escalabilidad en proyectos web y de software.
- Elemento:


## ¿Cómo se comunican los componentes? Props y atributos

- `Props` son los parametros que resiven los componentes, internamente son un objeto


# Emitiendo Eventos entre componentes

En React, la comunicación entre componentes se realiza principalmente a través de la propagación de datos desde los componentes padres a los componentes hijos utilizando props. Sin embargo, para comunicar eventos desde un componente hijo a un componente padre, se utiliza un patrón conocido como "levantamiento de estado" (lifting state up).

Aquí hay un ejemplo de cómo puedes emitir eventos desde un componente hijo a un componente padre en React:

1. **Componente Padre (`ParentComponent.js`):**

```jsx
import React, { useState } from 'react';
import ChildComponent from './ChildComponent';

function ParentComponent() {
  const [message, setMessage] = useState('');

  const handleChildEvent = (eventData) => {
    setMessage(eventData);
  };

  return (
    <div>
      <h1>Componente Padre</h1>
      <p>Mensaje del componente hijo: {message}</p>
      <ChildComponent onChildEvent={handleChildEvent} />
    </div>
  );
}

export default ParentComponent;
```

2. **Componente Hijo (`ChildComponent.js`):**

```jsx
import React from 'react';

function ChildComponent(props) {
  const handleClick = () => {
    const eventData = '¡Hola desde el componente hijo!';
    props.onChildEvent(eventData);
  };

  return (
    <div>
      <h2>Componente Hijo</h2>
      <button onClick={handleClick}>Enviar Mensaje al Padre</button>
    </div>
  );
}

export default ChildComponent;
```

En este ejemplo, el componente hijo `ChildComponent` recibe una prop llamada `onChildEvent`, que es una función pasada desde el componente padre. Cuando se hace clic en el botón en el componente hijo, se llama a la función `handleClick`, que emite un evento al componente padre a través de la función `onChildEvent`, pasando un mensaje como datos del evento.

El componente padre, `ParentComponent`, define una función `handleChildEvent` que se utiliza para actualizar el estado con el mensaje recibido del componente hijo. Luego, el mensaje actualizado se muestra en el componente padre.

Este patrón permite que el componente hijo emita eventos y notifique al componente padre sobre ciertas acciones. Es importante destacar que el componente padre controla el estado y la lógica relacionada con los eventos, mientras que el componente hijo simplemente llama a la función proporcionada por el padre para notificar eventos.